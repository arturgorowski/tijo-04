public class Main {
    private long getFactorial(final long digit) throws TooLargeException, NegativeException {
            // prosze zaimplementowac algorytm wyliczania silni
            // prosze dodac obsluge nastepujacych bledow:
            // * zabezpieczenie przed wprowadzaniem ujemnych liczb
            // * zabezpieczenie przed liczbami, dla ktorych silnia nie zostanie policzona (long).

            if( digit < 0 ) {
                throw new NegativeException();
            }

            if( digit > 49 ) {
                throw new TooLargeException();
            }

            long factorial = 1;
            for (long i = digit; i > 0; --i) {
                factorial *= i;
            }
            return factorial;
    }

    public static void main(String[] args) {
            long digit = 3;
            // metoda main powinna wyswietlac rowniez informacje na standardowym wyjsciu gdy wystapia bledy
            // w metodzie getFactorial - prosze zmodyfikowac kod.
            Main main = new Main();

            try {
                long factorial = main.getFactorial(digit);
                System.out.printf("Silnia(%d) = %d \n", digit, factorial);
            } catch (TooLargeException e) {
                System.out.println("Liczba jest zbyt duża!");
            } catch (NegativeException e) {
                System.out.println("Liczba nie może być ujemna!");
            }
    }
}